CC=gcc
CFLAGS=-Wall 
LIBS=-lm 

zad1: zad1.o zad2.o zad1_sd.o zad1_median.o zad1_mean.o sort_array.o
	$(CC) $(CFLAGS) -o zad2 zad2.o zad1_sd.o zad1_median.o zad2_mean.o sort_array.o $(LIBS)

zad1.o: zad1.c
	$(CC) $(CFLAGS) -c zad2.c
	
zad2.o: zad2.c
	$(CC) $(CFLAGS) -c zad2.c

zad1_mean.o: zad1_mean.c
	$(CC) $(CFLAGS) -c zad1_mean.c

zad1_median.o: zad1_median.c
	$(CC) $(CFLAGS) -c zad1_median.c

zad1_sd.o: zad1_sd.c
	$(CC) $(CFLAGS) -c zad1_sd.c

sort_array.o: sort_array.c
	$(CC) $(CFLAGS) -c sort_array.c
