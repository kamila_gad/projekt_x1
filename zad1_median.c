#include "zad1_median.h"
#include "sort_array.h"
float median(float *array, unsigned int elements) {
    sort_array(array, elements);
    float median = 0.0f;
    if (elements % 2 == 0)
        median = (array[(elements - 1) / 2] + array[elements / 2]) / 2.0f;
    else
        median = array[elements / 2];
    return median;
}