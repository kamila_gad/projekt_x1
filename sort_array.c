#include "sort_array.h"
void sort_array(float *array, unsigned int elements) {
    int index = 0, element = 0;
    float temp = 0.0f;

    for (index = 0; index < elements; index++) {
        for (element = 0; element < elements - 1; element++) {
            if (array[element] > array[element + 1]) {
                temp = array[element];
                array[element] = array[element + 1];
                array[element + 1] = temp;
            }
        }
    }
    return;
}