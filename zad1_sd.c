#include "zad1_sd.h"
#include <math.h>

float deviation(float *array, unsigned int elements) {
    int index = 0;
    float sum = 0.0f, variation = 0.0f, daviation_result = 0.0f;
    for (index = 0, sum = 0; index < elements; index++)
        sum += pow(array[index] - elements, 2);

    variation = sum / (float) elements;
    daviation_result = sqrt(variation);
    return daviation_result;
}